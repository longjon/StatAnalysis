Project with tests
==============================

This project is a test project that should be built against StatAnalysis as parent project. 
It contains various tests to ensure that the parent project StatAnalysis works as intended.
The reasoning for including an entire test project in this repository is that it implicitly 
tests StatAnalysis' intended feature of being used as a parent project, i.e. if this project 
builds succesfully with StatAnalysis as parent project that confirms StatAnalysis was likely
also built correctly.

Adding a test
----------------------

Ideally, each software shipped with the StatAnalysis release should be tested in this project.
In order to add a test for a software, simply add a bash script to the subfolder corresponding
to the software that is being tested. CMake will then automatically discover this bash script
and run it whenever the user calls `ctest`. See for example `StatAnalysis/TestProject/tests/ROOT`.

After following these steps, the test should be executed automatically as part of the CI. The
CI will also just execute `ctest`. To confirm that the test was executed successfully, go to the 
CI logs and see whether it indeed prints the name of the added test.

Checking test results
----------------------

The ROOT test folder also contains a file with truth results, `results_correct.csv`, while the 
test outputs a file, `results.csv`, with the same format as `results_correct.csv`. By comparing
these two csv files the testing framework ensures correct output is being generated in the test.
This is not necessary for all tests, but it is in general a good idea to check whether the
output results of any tests are correct.

For the ROOT test, the bash test script calls a python file `compare.py` to actually compare the
csv outputs, which will return 1 when it noticed a difference. This is just a suggestion, other
options for comparing results are also possible. 
