# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration

# Set up the project.
cmake_minimum_required( VERSION 3.6 )
set(CMAKE_FIND_NO_INSTALL_PREFIX TRUE) # don't add installation location to find path
project( StatAnalysis VERSION 999.999.999 LANGUAGES C CXX )

set(CMAKE_CXX_STANDARD 17 CACHE STRING "Default value for CXX_STANDARD property of targets") # Default C++17 standard

# set (CMAKE_FIND_PACKAGE_PREFER_CONFIG TRUE) # this is here so that if using ROOT from local install its picked up

# Set where to pick up AtlasCMake and AtlasLCG from.
set( AtlasCMake_DIR
   "${CMAKE_SOURCE_DIR}/Externals/atlasexternals/Build/AtlasCMake"
   CACHE PATH "Location of AtlasCMake" )
set( LCG_DIR "${CMAKE_SOURCE_DIR}/Externals/atlasexternals/Build/AtlasLCG"
   CACHE PATH "Location of AtlasLCG" )
mark_as_advanced( AtlasCMake_DIR LCG_DIR )

# Find the ATLAS CMake code.
find_package( AtlasCMake REQUIRED )

# Set up the AtlasLCG modules, without setting up an actual LCG release.
set( LCG_VERSION_POSTFIX "" CACHE STRING "The LCG version postfix to use" )
set( LCG_VERSION_NUMBER 0 CACHE STRING "The LCG version number to use" )
find_package( LCG ${LCG_VERSION_NUMBER} REQUIRED )

# Make CMake aware of the files in the cmake/ subdirectory.
list( INSERT CMAKE_MODULE_PATH 0 ${CMAKE_SOURCE_DIR}/cmake )


message(STATUS "Installation location: ${CMAKE_INSTALL_PREFIX}")

# Load the project's build options.
add_subdirectory(ProjectOptions)

# Set up CTest.
atlas_ctest_setup()

# Set up the project with all of its packages.
atlas_project()

# Generate the environment setup for the build directory.
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )

# Generate a relocatable environment setup script that will be installed with
# the release.
lcg_generate_env(
   SH_FILE ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/env_setup_install.sh
   REPLACE "${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}"
           "\${${CMAKE_PROJECT_NAME}_DIR}" )
install( FILES ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/env_setup_install.sh
   DESTINATION . )

# Install the project specific CMake files.
install( FILES cmake/SetupPython.cmake cmake/SetupROOT.cmake
   DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules )

# Generate and install the version motd
# generate twice: once with and once without @ONLY restriction ... will use the former file to drop in
# versions of sw we are able to detect
configure_file( ${CMAKE_SOURCE_DIR}/cmake/scripts/motd.in ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/motd.tmp1 @ONLY  )
configure_file( ${CMAKE_SOURCE_DIR}/cmake/scripts/motd.in ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/motd )
add_custom_target(StatAnalysis_motd ALL
        COMMENT "Generating StatAnalysis motd ..."
        DEPENDS Package_xRooFit Package_TRExFitter Package_RooFitExtensions Package_RooUnfold Package_workspaceCombiner Package_xmlAnaWSBuilder Package_quickFit Package_BootstrapGenerator Package_CommonStatTools Package_CMSCombine
        COMMAND ${CMAKE_COMMAND} -DOUTPUT_FILE=${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/motd.tmp2
        -DINPUT_FILE=${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/motd.tmp1
        -DBUILD_DIR=${CMAKE_BINARY_DIR}
        -P ${CMAKE_CURRENT_SOURCE_DIR}/cmake/versioning.cmake
        COMMAND ${CMAKE_COMMAND} -E copy_if_different ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/motd.tmp2
        ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/motd
        COMMAND ${CMAKE_COMMAND} -E remove ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/motd.tmp2)
install( FILES ${CMAKE_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/motd DESTINATION etc )
install( FILES ${CMAKE_SOURCE_DIR}/cmake/scripts/istartup.py DESTINATION etc )

if("${CMAKE_INSTALL_PREFIX}" STREQUAL
        "/${CMAKE_PROJECT_NAME}/${CMAKE_PROJECT_VERSION}/InstallArea/${ATLAS_PLATFORM}")
    # Package up the release using CPack.
    atlas_cpack_setup()
endif()
