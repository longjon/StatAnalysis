# Copyright (C) 2002-2021 CERN for the benefit of the ATLAS collaboration
#
# Package building CMSCombine for StatAnalysis releases
#

# The name of the package:
atlas_subdir( CMSCombine )

if( STATANA_CMSCOMBINE_VERSION STREQUAL "" )
    message(STATUS "NOT BUILDING: CMSCombine")
    return()
endif()

find_package(Eigen QUIET)
if(NOT EIGEN_FOUND AND NOT ATLAS_BUILD_EIGEN)
    message(STATUS "WARNING: Cannot build CMSCombine without Eigen")
    return()
endif()

# Set token if it exists, otherwise use no token
if(CI_JOB_TOKEN)
    set( _token "gitlab-ci-token:${CI_JOB_TOKEN}@" )
else()
    set( _token "" )
endif()

set( STATANA_CMSCOMBINE_REPOSITORY "https://github.com/will-cern/HiggsAnalysis-CombinedLimit.git" CACHE STRING "Repository of cmscombine" )

set( _buildDir ${CMAKE_CURRENT_BINARY_DIR}${CMAKE_FILES_DIRECTORY}/CMSCombineBuild )

if(STATANA_VERBOSE)
    set(_logging OFF)
else()
    set(_logging ON)
endif()

# Build lwtnn for the build area:
ExternalProject_Add( CMSCombine
        PREFIX ${CMAKE_BINARY_DIR}
        INSTALL_DIR ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}
        GIT_REPOSITORY ${STATANA_CMSCOMBINE_REPOSITORY}
        GIT_TAG ${STATANA_CMSCOMBINE_VERSION}
        BUILD_ALWAYS ${TRACK_CHANGES}
        CMAKE_CACHE_ARGS
        -DCMAKE_INSTALL_PREFIX:PATH=${_buildDir}
        -DCMAKE_PREFIX_PATH:PATH=${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}  # ensures will discover the release's version of ROOT
        -DMODIFY_ROOTMAP:BOOL=TRUE
        LOG_DOWNLOAD ${_logging} LOG_CONFIGURE ${_logging} LOG_BUILD ${_logging} LOG_INSTALL ${_logging}
        LOG_OUTPUT_ON_FAILURE 1
        UPDATE_COMMAND "" # needed for next line to work
        UPDATE_DISCONNECTED TRUE) # skips reconfigure+build if just rerunning.
ExternalProject_Add_Step( CMSCombine buildinstall
        COMMAND ${CMAKE_COMMAND} -E copy_directory ${_buildDir} <INSTALL_DIR>
        COMMENT "Installing CMSCombine into the build area"
        DEPENDEES install
        )

if( ATLAS_BUILD_ROOT )
    add_dependencies ( CMSCombine ROOT )
endif()

add_dependencies( CMSCombine Eigen Boost )


# Install CMSCombine:
install( DIRECTORY ${_buildDir}/
        DESTINATION . USE_SOURCE_PERMISSIONS OPTIONAL )

install( FILES cmake/FindCMSCombine.cmake
        DESTINATION ${CMAKE_INSTALL_CMAKEDIR}/modules OPTIONAL )
