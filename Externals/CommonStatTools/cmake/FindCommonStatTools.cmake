# Copyright (C) 2002-2019 CERN for the benefit of the ATLAS collaboration
#
# Locate the CommonStatTools external package.
#
# Defines:
#  COMMONSTATTOOLS_FOUND
#  COMMONSTATTOOLS_INCLUDE_DIR
#  COMMONSTATTOOLS_INCLUDE_DIRS
#  COMMONSTATTOOLS_LIBRARIES
#  COMMONSTATTOOLS_LIBRARY_DIRS
#
# The user can set COMMONSTATTOOLS_ATROOT to guide the script.
#

# Include the helper code:
include( AtlasInternals )

# Declare the module:
atlas_external_module( NAME CommonStatTools
        INCLUDE_SUFFIXES include INCLUDE_NAMES CommonStatTools
        LIBRARY_SUFFIXES lib
        COMPULSORY_COMPONENTS CommonStatTools )

# Handle the standard find_package arguments:
include( FindPackageHandleStandardArgs )
find_package_handle_standard_args( CommonStatTools DEFAULT_MSG COMMONSTATTOOLS_INCLUDE_DIRS
        COMMONSTATTOOLS_LIBRARIES )
mark_as_advanced( COMMONSTATTOOLS_FOUND COMMONSTATTOOLS_INCLUDE_DIR COMMONSTATTOOLS_INCLUDE_DIRS
        COMMONSTATTOOLS_LIBRARIES COMMONSTATTOOLS_LIBRARY_DIRS )


