# Copyright (C) 2002-2020 CERN for the benefit of the ATLAS collaboration
#
# File filtering which "packages" to build as part of the atlas-stats project.
#

# Select which packages to pick up from atlasexternals.
+ Externals/atlasexternals/External/Davix
+ Externals/atlasexternals/External/dcap
# + Externals/atlasexternals/External/Python
# + Externals/atlasexternals/External/TBB -- ROOT can build for us
# eigen needed for cmscombine
+ Externals/atlasexternals/External/Eigen
# Do not pick up anything else from atlasexternals.
- Externals/atlasexternals.*

# Do not pick up the WorkDir project.
- WorkDir
# nor the ProjectOptions
- ProjectOptions

# Everything else (packages held in this repository) is fair game...
